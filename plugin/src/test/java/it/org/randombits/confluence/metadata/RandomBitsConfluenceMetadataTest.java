package it.org.randombits.confluence.metadata;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.atlassian.plugin.util.ClassLoaderUtils.getResourceAsStream;

/**
 * @author HengHwa
 * @since 1.0.7.20141009
 */
public class RandomBitsConfluenceMetadataTest extends AbstractWebDriverTest {
    @After public void tearDown() throws Exception {
        rpc.clearIndexQueue();
        rpc.flushIndexQueue();
        rpc.flushEdgeIndexQueue();
    }

    @Test public void testRandomBitsPluginIsInstalled() throws Exception {
        assert new AddOnTest().isInstalled(
            product.login(User.ADMIN, AddOnOSGIPage.class),
                "Scaffolding Metadata Plugin"
        );
    }
}
