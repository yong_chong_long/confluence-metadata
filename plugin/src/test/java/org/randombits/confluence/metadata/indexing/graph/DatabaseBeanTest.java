package org.randombits.confluence.metadata.indexing.graph;

import com.atlassian.confluence.setup.BootstrapManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static java.io.File.separator;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author HengHwa
 * @since 7.0.0.20150226
 */
@RunWith(MockitoJUnitRunner.class)
public class DatabaseBeanTest {

    private DatabaseBean $;

    @Mock BootstrapManager bootstrapManager;

    private File path = new File(randomAlphanumeric(5));

    @Before public void setUp() throws Exception {
        $ = new DatabaseBean();
        $.setBootstrapManager(bootstrapManager);

        when(bootstrapManager.getLocalHome()).thenReturn(path);
    }

    @Test public void returnExpectedDatabaseDirectoryPath() {
        String dbPath = "plocal:" + bootstrapManager.getLocalHome() + separator + "com.servicerocket.confluence.metadata" + separator + $.DATABASE_NAME;
        assertEquals(
                "Should return string: " + dbPath,
                $.getDatabaseDirectory(),
                dbPath
        );
    }
}
