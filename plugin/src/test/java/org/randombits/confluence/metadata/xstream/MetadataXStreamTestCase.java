/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.xstream;

import com.thoughtworks.xstream.core.BaseException;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Attribute;
import org.dom4j.Node;
import org.dom4j.Text;
import org.dom4j.io.SAXReader;
import org.randombits.confluence.metadata.MetadataMap;

/**
 * Tests the {@link org.randombits.confluence.metadata.xstream.MetadataXStream} class.
 * 
 * @author David Peterson
 */
public class MetadataXStreamTestCase extends TestCase {
    /**
     * Dummy test while no other tests are runnable.
     * 
     * @throws org.dom4j.DocumentException
     *             if there is an error while reading the XML file.
     */
    public void testBasic() throws DocumentException {
        MetadataXStream xstream = new MetadataXStream();
        try {
            MetadataMap metadata = ( MetadataMap ) fromXML( xstream, "basic.xml" );
            Object object = metadata.get( "first" );
            Assert.assertTrue( "String", object instanceof String );
            String value = ( String ) object;
            Assert.assertEquals( "first", "This is first.", value );

            SAXReader reader = new SAXReader();
            reader.setMergeAdjacentText( true );

            Document doc = reader.read( getClass().getResourceAsStream( "basic.xml" ) );

            String xml = xstream.toXML( metadata );
            Document newDoc = reader.read( new StringReader( xml ) );
            compareElements( doc.getRootElement(), newDoc.getRootElement() );

        } catch ( BaseException e ) {
            e.printStackTrace();
            Assert.fail( "BaseException thrown: " + e );
        }
    }

    /*
     * TODO: Re-enable this once Confluence updates to XStream 1.2 public void
     * testPlaceholderSimple() { MetadataXStream xstream = new
     * MetadataXStream(); try { MetadataMap metadata = (MetadataMap)
     * fromXML(xstream, "placeholder-simple.xml"); Object object =
     * metadata.get("simple"); Assert.assertTrue("Placeholder", object
     * instanceof Placeholder); Placeholder uo = (Placeholder) object;
     * Assert.assertEquals("name", "simple", uo.getName()); } catch
     * (BaseException e) { Assert.fail("BaseException thrown: " + e); } }
     * 
     * public void testPlaceholderComplex() throws DocumentException {
     * MetadataXStream xstream = new MetadataXStream(); try { MetadataMap
     * metadata = (MetadataMap) fromXML(xstream, "placeholder-complex.xml");
     * Object object = metadata.get("complex"); Assert.assertTrue("Placeholder",
     * object instanceof Placeholder); Placeholder uo = (Placeholder) object;
     * Assert.assertEquals("name", "complex", uo.getName());
     * 
     * SAXReader reader = new SAXReader();
     * 
     * Document doc =
     * reader.read(getClass().getResourceAsStream("complex.xml")); //
     * compareElements(doc.getRootElement(), uo.getElement());
     * 
     * String xml = xstream.toXML(metadata); Document newDoc = reader.read(new
     * StringReader(xml)); compareElements(doc.getRootElement(),
     * newDoc.getRootElement()); } catch (BaseException e) {
     * Assert.fail("BaseException thrown: " + e); } }
     */

    private void compareElements( Element original, Element generated ) {
        Assert.assertEquals( "name", original.getQName(), generated.getQName() );

        // Compare attributes
        Assert.assertEquals( "attribute count", original.attributeCount(), generated.attributeCount() );
        Iterator<Attribute> atts = original.attributeIterator();
        while ( atts.hasNext() ) {
            Attribute origAtt = atts.next();
            Attribute genAtt = generated.attribute( origAtt.getQName() );
            Assert
                    .assertEquals( "attribute qualified name", origAtt.getQualifiedName(), genAtt
                            .getQualifiedName() );
            Assert.assertEquals( "attribute value", origAtt.getValue(), genAtt.getValue() );
        }

        // Compare contents
        Assert.assertEquals( "node count", original.nodeCount(), generated.nodeCount() );
        Iterator<Node> origNodes = original.nodeIterator();
        Iterator<Node> genNodes = generated.nodeIterator();
        while ( origNodes.hasNext() && genNodes.hasNext() ) {
            Node origNode = origNodes.next();
            Node genNode = genNodes.next();

            if ( origNode instanceof Text && genNode instanceof Text ) {
                // ignore whitespace.
                Assert.assertEquals( "text node", origNode.getText().trim(), genNode.getText().trim() );
            } else if ( origNode instanceof Element && genNode instanceof Element ) {
                compareElements( ( Element ) origNode, ( Element ) genNode );
            } else {
                Assert.assertEquals( "node type", origNode.getNodeTypeName(), genNode.getNodeTypeName() );
                Assert.assertEquals( "node value", origNode, genNode );
            }
        }
    }

    private Object fromXML( MetadataXStream xstream, String resourceName ) {
        return xstream.fromXML( new InputStreamReader( getClass().getResourceAsStream( resourceName ) ) );
    }
}
