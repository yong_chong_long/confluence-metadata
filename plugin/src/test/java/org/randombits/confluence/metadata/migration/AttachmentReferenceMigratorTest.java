package org.randombits.confluence.metadata.migration;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.confluence.metadata.MetadataMap;
import org.randombits.confluence.metadata.reference.AttachmentReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link org.randombits.confluence.metadata.migration.AttachmentReferenceMigrator} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class AttachmentReferenceMigratorTest {

    AttachmentReferenceMigrator $;
    AttachmentReference attachmentReference;
    List list;
    Map map;

    @Mock ContentEntityObject contentEntityObject;
    @Mock Attachment attachment;

    @Before public void setup() {
        when(contentEntityObject.getId()).thenReturn(123456L);
        when(attachment.getContent()).thenReturn(contentEntityObject);
        when(attachment.getFileName()).thenReturn("testFileName.mp3");

        attachmentReference = new AttachmentReference(attachment);

        list = new ArrayList();
        list.add(attachmentReference);

        map = new MetadataMap();
        map.put("testKey", list);

        $ = new AttachmentReferenceMigrator();
    }

    @Test public void shouldUpdateAttachmentId() {
        when(attachment.getId()).thenReturn(0L);
        assertThat(((AttachmentReference)((List) map.get("testKey")).get(0)).getAttachmentId(), is(0L));

        when(attachment.getId()).thenReturn(456789L);
        $.process(map, attachment);
        assertThat(((AttachmentReference)((List) map.get("testKey")).get(0)).getAttachmentId(), is(456789L));
    }
}
