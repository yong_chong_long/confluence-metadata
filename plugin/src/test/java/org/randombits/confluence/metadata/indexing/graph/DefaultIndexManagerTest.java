package org.randombits.confluence.metadata.indexing.graph;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.event.api.EventPublisher;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.confluence.metadata.indexing.FieldNotFoundException;
import org.randombits.confluence.metadata.indexing.graph.vertex.Collection;
import org.randombits.confluence.metadata.indexing.graph.vertex.Content;
import org.randombits.confluence.metadata.indexing.graph.vertex.KeyValueField;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author HengHwa
 * @since 7.0.0.20150226
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultIndexManagerTest {

    @Mock EventPublisher eventPublisher;
    @Mock BootstrapManager bootstrapManager;
    @Mock OClass clazz;
    @Mock OrientVertex orientVertex;
    @Mock OrientGraph orientGraph;
    @Mock DatabaseBean databaseBean;

    private DefaultIndexManager $;
    private Map <String, Object> metadataString1 = new HashMap<>();
    private Map <String, Object> metadataString2 = new HashMap<>();
    private Map <String, Object> metadataCollection1 = new HashMap<>();
    private String contentId = randomNumeric(5);
    private static String FIELD1 = randomAlphabetic(4);
    private static String FIELD2 = randomAlphabetic(4);

    @Before public void setUp() throws Exception {
        $ = new DefaultIndexManager(eventPublisher);

        $.setDatabaseBean(databaseBean);

        metadataCollection1.put(randomAlphabetic(4), randomAlphanumeric(15));
        metadataCollection1.put(randomAlphabetic(4), randomAlphanumeric(15));

        when(databaseBean.getGraph()).thenReturn(orientGraph);
        when(orientGraph.addVertex("class:" + Content.CLASS_NAME, Content.CONTENT_ID.toCanonicalValue(), contentId)).thenReturn(orientVertex);
    }

    @Test (expected = IllegalArgumentException.class)
    public void shouldReturnIllegalArgumentExceptionForNotSupportedType() throws FieldNotFoundException {
        $.query(randomAlphanumeric(5), randomAlphanumeric(5), Logger.class);
    }

    @Test
    public void shouldAddStringFieldToGraph() {
        metadataString1.put(FIELD1, randomAlphanumeric(15));
        metadataString1.put(FIELD2, randomAlphanumeric(15));

        $.buildIndex(contentId, metadataString1);

        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + Content.CLASS_NAME, Content.CONTENT_ID.toCanonicalValue(), contentId);
        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + KeyValueField.CLASS_NAME, KeyValueField.NAME.toCanonicalValue(), FIELD1, KeyValueField.VALUE.toCanonicalValue(), metadataString1.get(FIELD1));
        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + KeyValueField.CLASS_NAME, KeyValueField.NAME.toCanonicalValue(), FIELD2, KeyValueField.VALUE.toCanonicalValue(), metadataString1.get(FIELD2));

        verify(databaseBean.getGraph(), times(2)).addEdge(null, orientVertex, null, Content.EDGE_CONTAINS);
    }

    @Test
    public void shouldAddCollectionFieldToGraph() {
        metadataString2.put(FIELD1, metadataCollection1);
        metadataString2.put(FIELD2, metadataCollection1);

        $.buildIndex(contentId, metadataString2);

        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + Content.CLASS_NAME, Content.CONTENT_ID.toCanonicalValue(), contentId);
        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + Collection.CLASS_NAME, Collection.NAME.toCanonicalValue(), FIELD1, KeyValueField.VALUE.toCanonicalValue(), FIELD1);
        verify(databaseBean.getGraph(), times(1)).addVertex("class:" + Collection.CLASS_NAME, Collection.NAME.toCanonicalValue(), FIELD2, KeyValueField.VALUE.toCanonicalValue(), FIELD2);
        verify(databaseBean.getGraph(), times(2)).addEdge(null, orientVertex, null, Content.EDGE_CONTAINS);
    }

}
