/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.confluence.metadata;

import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.confluence.metadata.indexing.FieldNotFoundException;
import org.randombits.storage.Aliasable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class helps keep track of scaffolding data.
 */
public interface MetadataManager extends Aliasable {

    void addTypeHandler( TypeHandler handler );

    void removeTypeHandler( TypeHandler handler );

    void saveNextData( MetadataStorage data );

    MetadataStorage loadReadableData( ContentEntityObject content );

    MetadataStorage loadWritableData( ContentEntityObject content, int version );

    MetadataStorage loadWritableData( ContentEntityObject content );

    MetadataStorage loadNewWritableData( ContentEntityObject contentTo, ContentEntityObject contentFrom );

    String loadDataXML( ContentEntityObject content );

    void saveData( MetadataStorage data, boolean currentContentVersion );

    void saveSetData(MetadataStorage data);

    void clearCache();

    void clearNextData( ContentEntityObject content );

    MetadataStorage asMetadataStorage( Map<String, Object> map );

    /**
     * Attempts to convert the object from its original value into a 'storable' value
     * that can be persisted safely. This is dependent on what {@link TypeHandler}s
     * are available via the plugin system.
     * <p/>
     * <p>The returned value may be the exact same object, if it is directly storable, or
     * it may be a 'reference' instance that points to the original.</p>
     *
     * @param original The original object value.
     * @return The storable object value, or null if it could not be stored safely.
     * @throws TypeConversionException if there is a problem while converting.
     */
    Object toStorable( Object original ) throws TypeConversionException;

    /**
     * Converts the object from a 'storable' instance to the 'original'. This will
     * probably not be the same instance of the original, but it will be a faithful
     * reconstruction.
     *
     * @param storable The storable value.
     * @return The original value, or null if the storable value is not supported.
     * @throws TypeConversionException if there is an error while converting.
     */
    Object fromStorable( Object storable ) throws TypeConversionException;

    /**
     * Checks if the value is storable.
     *
     * @param value The value to check.
     * @return <code>true</code> if the value can be stored as metadata.
     */
    boolean isStorable( Object value );

    /**
     * Converts the specified object to the XML format used by Scaffolding for storage.
     * It takes into account any custom storage handling for the object.
     *
     * @param value The value to convert.
     * @return The XML value, or null if the value is not supported.
     * @throws TypeConversionException if there are any issues.
     */
    String toXML( Object value ) throws TypeConversionException;

    /**
     * Converts the specified XML value to its original value (not a 'storable' value).
     *
     * @param xml The XML to convert.
     * @return The original value.
     * @throws TypeConversionException if there are any issues.
     */
    Object fromXML( String xml ) throws TypeConversionException;

    /**
     * Query a value from index using content ID and field name.
     *
     * @param contentId
     * @param fieldName
     * @param clazz Type of the field to look for, e.g. passing String.class will limit the query to string type fields.
     * @return Value of type T.
     * @throws IllegalArgumentException when {@literal clazz} type is not supported.
     * @throws FieldNotFoundException when fieldName cannot be found.
     */
    <T> T queryIndex(String contentId, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException;

    /**
     * Query a value from index using content ID and field name and parent field name.
     *
     * @param contentId
     * @param path
     * @param fieldName
     * @param clazz Type of the field to look for, e.g. passing String.class will limit the query to string type fields.
     * @return Value of type T.
     * @throws IllegalArgumentException when {@literal clazz} type is not supported.
     * @throws FieldNotFoundException when fieldName cannot be found.
     */
    <T> T queryIndex(String contentId, List<String> path, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException;
}
