package org.randombits.confluence.metadata.impl.handler;

import org.randombits.confluence.metadata.TypeHandler;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Handles a type which is stored directly without modification.
 */
public class DirectTypeHandler implements TypeHandler {

    private final Set<Class<?>> types;

    public DirectTypeHandler( Class<?>... types ) {
        this( Arrays.asList( types ) );
    }

    public DirectTypeHandler( Collection<Class<?>> types ) {
        this.types = new HashSet<Class<?>>( types );
    }

    public boolean supportsOriginal( Object original ) {
        return supportsType( original );
    }

    public boolean supportsStorable( Object stored ) {
        return supportsType( stored );
    }

    private boolean supportsType( Object value ) {
        for ( Class<?> type : types ) {
            if ( type.isInstance( value ) )
                return true;
        }
        return false;
    }

    public Object getOriginal( Object stored ) {
        // Assumes 'supportsStored' has been called.
        return stored;
    }

    public Object getStorable( Object value ) {
        // Assumes 'supportsOriginal' has been called.
        return value;
    }
}
