package org.randombits.confluence.metadata.impl.handler;

import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.confluence.metadata.AbstractTypeHandler;
import org.randombits.confluence.metadata.reference.ContentReference;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Handles {@link org.randombits.confluence.metadata.reference.ContentReference}s.
 */
public class ContentEntityObjectHandler extends AbstractTypeHandler<ContentEntityObject, ContentReference> {

    public static final String LEGACY_ALIAS = "ContentOption";

    public static final String ALIAS = "ContentReference";

    private final ContentEntityManager contentEntityManager;

    public ContentEntityObjectHandler( @Qualifier("contentEntityManager") ContentEntityManager contentEntityManager ) {
        super( ContentEntityObject.class, ContentReference.class );
        this.contentEntityManager = contentEntityManager;
    }

    @Override
    protected ContentEntityObject doGetOriginal( ContentReference stored ) {
        return contentEntityManager.getById( stored.getId() );
    }

    @Override
    protected ContentReference doGetStored( ContentEntityObject original ) {
        return new ContentReference( original );
    }

    @Override
    protected String getAlias() {
        return ALIAS;
    }
}
