/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.confluence.metadata.expression;

import org.randombits.math.eval.Cases;
import org.randombits.math.eval.ExpressionCommand;
import org.randombits.math.eval.ExpressionProgram;
import org.randombits.math.eval.StackOfDouble;
import org.randombits.math.eval.Variable;
import org.randombits.storage.Storage;

/**
 * A command which will sum the specified fields in the table when applied.
 * 
 * @author David Peterson
 */
public class SumTableCommand implements ExpressionCommand {
    /**
     * 
     */
    private static final long serialVersionUID = -2438215390705484823L;

    private Storage fields;

    private String tableName, fieldName;

    private String rowCountField;

    SumTableCommand( String tableName, String fieldName, String rowCountField, Storage fields, ExpressionProgram e ) {
        // Constructor.
        this.tableName = tableName;
        this.fieldName = fieldName;
        this.rowCountField = rowCountField;
        this.fields = fields;
    }

    public void apply( StackOfDouble stack, Cases cases ) {
        double sum = 0;

        // Push into the table
        fields.openBox( tableName );

        int rowCount = fields.getInteger( rowCountField, 0 );

        for ( int i = 0; i < rowCount; i++ ) {
            fields.openBox( String.valueOf( i ) );

            Number value = fields.getNumber( fieldName, null );

            sum += ( value != null ) ? value.doubleValue() : 0.0d;

            fields.closeBox();
        }

        // Get back out of the table space.
        fields.closeBox();

        stack.push( sum );
    }

    public void compileDerivative( ExpressionProgram prog, int myIndex, ExpressionProgram deriv, Variable wrt ) {
        deriv.addConstant( 0 );
    }

    public int extent( ExpressionProgram prog, int myIndex ) {
        return 1; // Upper + lower limits + this object.
    }

    public boolean dependsOn( Variable x ) {
        return false;
    }

    public void appendOutputString( ExpressionProgram prog, int myIndex, StringBuffer buffer ) {
        buffer.append( "sumtable(\"" );
        buffer.append( tableName );
        buffer.append( "\", \"" );
        buffer.append( fieldName );
        buffer.append( "\")" );
    }

}