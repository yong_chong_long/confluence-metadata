package org.randombits.confluence.metadata.indexing.graph.vertex;

/**
 * Vertex type: Collection
 * Represent collection type metadata fields.
 *
 * @author Fadli
 * @since 7.1.0.20150331
 */
public enum Collection {
    NAME ("name");

    public static final String CLASS_NAME = "Collection";

    private final String propertyName;

    Collection(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPrefixedPropertyKey() {
        return CLASS_NAME + "." + propertyName;
    }

    public String toCanonicalValue() {
        return propertyName;
    }
}
