package org.randombits.confluence.metadata.indexing.graph.vertex;

/**
 * Vertex type: KeyValueField
 * Represent string type metadata fields.
 *
 * @author LongYC
 * @since 7.0.0.20150211
 */
public enum KeyValueField {
    NAME ("name"),
    VALUE ("value");

    public static final String CLASS_NAME = "KeyValueField";

    private final String propertyName;

    KeyValueField(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPrefixedPropertyKey() {
        return CLASS_NAME + "." + propertyName;
    }

    public String toCanonicalValue() {
        return propertyName;
    }
}
