package org.randombits.confluence.metadata.indexing;

/**
 * Exception thrown when a field cannot be found in index.
 *
 * @author LongYC
 * @since 7.0.0.20150227
 */
public class FieldNotFoundException extends Exception {

    public FieldNotFoundException() {}

    public FieldNotFoundException(String message) {
        super(message);
    }

    public FieldNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldNotFoundException(Throwable cause) {
        super(cause);
    }
}
