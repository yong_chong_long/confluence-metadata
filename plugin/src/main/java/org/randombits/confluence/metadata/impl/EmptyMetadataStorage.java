/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.impl;

import org.randombits.confluence.metadata.MetadataStorage;
import org.randombits.storage.EmptyStorage;
import com.atlassian.confluence.core.ContentEntityObject;

import java.util.Date;
import java.util.Map;

/**
 * TODO: Document this class.
 *
 * @author David Peterson
 */
public class EmptyMetadataStorage extends EmptyStorage implements MetadataStorage
{
    /**
     * @return the ContentEntityObject the metadata is for.
     */
    public ContentEntityObject getContent()
    {
        return null;
    }

    /**
     * @return the version number of the metadata.
     */
    public int getVersion()
    {
        return 0;
    }

    /**
     * @return the username of the last modifier.
     */
    public String getLastModifierName()
    {
        return null;
    }

    /**
     * @return the date this version of the metadata was last edited.
     */
    public Date getLastModificationDate()
    {
        return null;
    }

    /**
     * Sets the last modifier's name.
     *
     * @param name the last modifier.
     */
    public void setLastModifierName(String name)
    {
    }

    /**
     * Sets the last date this version of metadata was modified.
     *
     * @param date the last modification date.
     */
    public void setLastModificationDate(Date date)
    {
    }

    /**
     * The user's comment about this version.
     *
     * @param comment The comment.
     */
    public void setComment(String comment)
    {
    }

    /**
     * Returns the user's comment. May be <code>null</code>.
     *
     * @return the comment.
     */
    public String getComment()
    {
        return null;
    }

    /**
     * @return the Map the data is stored in.
     */
    public Map<String, Object> getBaseMap()
    {
        return null;
    }

    /**
     * Returns <code>true</code> if the metadata has been modified since it was
     * last loaded/created.
     *
     * @return <code>true</code> if modified.
     */
    public boolean isModified()
    {
        return false;
    }
}
