package org.randombits.confluence.metadata.impl.handler;

import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.TypeConversionException;
import org.randombits.confluence.metadata.TypeHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Handling conversion between of list contents into storable values.
 */
public class ListHandler implements TypeHandler {

    private final MetadataManager metadataManager;

    public ListHandler(MetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    public boolean supportsOriginal(Object original) {
        return original instanceof List;
    }

    public boolean supportsStorable(Object stored) {
        return stored instanceof List;
    }

    public Object getOriginal(Object stored) throws TypeConversionException {
        List<?> storedList = (List<?>) stored;
        List<Object> originalList = new ArrayList<Object>();
        for (Object value : storedList) {
            originalList.add(metadataManager.fromStorable(value));
        }
        return originalList;
    }

    public Object getStorable(Object original) throws TypeConversionException {
        List<?> originalList = (List<?>) original;
        List<Object> storedList = new ArrayList<Object>();
        for (Object value : originalList) {
            storedList.add(metadataManager.toStorable(value));
        }
        return storedList;
    }
}
