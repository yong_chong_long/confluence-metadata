package org.randombits.confluence.metadata.indexing.graph.vertex;

/**
 * Vertex type: Content
 * Represent Confluence content.
 *
 * @author LongYC
 * @since 7.0.0.20150211
 */
public enum Content {
    CONTENT_ID ("contentId");

    public static final String CLASS_NAME = "Content";
    public static final String EDGE_CONTAINS = "contains";

    private final String propertyName;

    Content(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPrefixedPropertyKey() {
        return CLASS_NAME + "." + propertyName;
    }

    public String toCanonicalValue() {
        return propertyName;
    }
}
