package org.randombits.confluence.metadata;

/**
 * Thrown when there is an issue converting to or from 'original' and 'stored' types.
 */
public class TypeConversionException extends Exception {

    public TypeConversionException() {
    }

    public TypeConversionException( String s ) {
        super( s );
    }

    public TypeConversionException( String s, Throwable throwable ) {
        super( s, throwable );
    }

    public TypeConversionException( Throwable throwable ) {
        super( throwable );
    }
}
