package org.randombits.confluence.metadata.event;

/**
 * Interface for classes that want to emit MetadataUpdatedEvent.
 *
 * @author LongYC
 * @since 7.0.0.20150227
 */
public interface MetadataUpdatedEventEmitter {
}
