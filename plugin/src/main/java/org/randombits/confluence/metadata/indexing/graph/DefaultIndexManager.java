package org.randombits.confluence.metadata.indexing.graph;

import com.atlassian.confluence.event.events.cluster.ClusterEventWrapper;
import com.atlassian.event.Event;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.orientechnologies.orient.core.Orient;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Query;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.blueprints.impls.orient.OrientVertexQuery;
import org.apache.commons.lang3.StringUtils;
import org.randombits.confluence.metadata.event.MetadataUpdatedEvent;
import org.randombits.confluence.metadata.indexing.FieldNotFoundException;
import org.randombits.confluence.metadata.indexing.IndexManager;
import org.randombits.confluence.metadata.indexing.graph.vertex.Collection;
import org.randombits.confluence.metadata.indexing.graph.vertex.Content;
import org.randombits.confluence.metadata.indexing.graph.vertex.KeyValueField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of {@link org.randombits.confluence.metadata.indexing.IndexManager}.
 *
 * @author kaifung
 * @since 7.0.0.20150209
 */
@Component
public class DefaultIndexManager implements IndexManager, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(IndexManager.class);

    private final EventPublisher eventPublisher;

    private DatabaseBean databaseBean;

    public DefaultIndexManager(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        this.eventPublisher.register(this);
    }

    private void checkNotNull(Object result) throws FieldNotFoundException {
        if (result == null) {
            throw new FieldNotFoundException("Unable to find field.");
        }
    }

    @Override
    public <T> T query(String contentId, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException {
        return query(contentId, null, fieldName, clazz);
    }

    @Override
    public <T> T query(String contentId, List<String> path, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException {
        if (!String.class.equals(clazz)) {
            throw new IllegalArgumentException("Unable to query index with invalid type : " + clazz);
        }

        if (StringUtils.isEmpty(contentId) || StringUtils.isEmpty(fieldName)) {
            throw new IllegalArgumentException("Content ID and field name must not be empty.");
        }

        T result = null;

        // Retrieve Contents.
        for (Vertex content : databaseBean.getGraph().getVertices(Content.CONTENT_ID.getPrefixedPropertyKey(), contentId)) {
            // Look up for the specified field name.
            result = findVertex(content, fieldName, path);
        }
        checkNotNull(result);

        return result;
    }

    private <T> T findVertex(Vertex outVertex, String fieldName, List<String> path) {
        T result = null;

        for (Vertex vertex: outVertex.query().direction(Direction.OUT).labels(Content.EDGE_CONTAINS).vertices()) {
            if (result != null) {
                return result;
            } else if (((OrientVertex) vertex).getLabel().equals(Collection.CLASS_NAME) && !path.isEmpty()) {
                if (vertex.getProperty(Collection.NAME.toCanonicalValue()).equals(path.get(0))) {
                    path.remove(0);
                    result = findVertex(vertex, fieldName, path);
                }
            } else if (fieldName.equals(vertex.getProperty(KeyValueField.NAME.toCanonicalValue())) && path.isEmpty()) {
                return vertex.getProperty(KeyValueField.VALUE.toCanonicalValue());
            }
        }

        return result;
    }

    @Override
    public void buildIndex(String contentId, Map<String, Object> metadata) {
        Vertex content = replaceContentVertex(contentId);
        indexFields(content, metadata);

        databaseBean.getGraph().commit();
    }

    private Vertex replaceContentVertex(String contentId) {
        OrientGraph orientGraph = databaseBean.getGraph();

        if (orientGraph.countVertices() != 0) {
            for (Vertex content : orientGraph.getVertices(Content.CONTENT_ID.getPrefixedPropertyKey(), contentId)) {
                removeVertex(orientGraph, content);
            }
        }

        return orientGraph.addVertex("class:" + Content.CLASS_NAME, Content.CONTENT_ID.toCanonicalValue(), contentId);
    }

    private void removeVertex(OrientGraph orientGraph, Vertex vertex) {
        if (vertex.query().direction(Direction.OUT).labels(Content.EDGE_CONTAINS).count() != 0) {
            for (Vertex content : vertex.query().direction(Direction.OUT).labels(Content.EDGE_CONTAINS).vertices()) {
                removeVertex(orientGraph, content);
            }
        }

        orientGraph.removeVertex(vertex);
    }

    private void indexFields(Vertex outVertex, Map<String, ?> metadata) {
        OrientGraph orientGraph = databaseBean.getGraph();

        for (Map.Entry<String, ?> entry : metadata.entrySet()) {
            if (entry.getValue() instanceof Map) {
                Vertex inVertex = orientGraph.addVertex("class:" + Collection.CLASS_NAME,
                    Collection.NAME.toCanonicalValue(), entry.getKey(),
                    KeyValueField.VALUE.toCanonicalValue(), entry.getKey()
                );

                orientGraph.addEdge(null, outVertex, inVertex, Content.EDGE_CONTAINS);
                indexFields(inVertex, (Map<String, ?>) entry.getValue());
            } else {
                Vertex inVertex = orientGraph.addVertex("class:" + KeyValueField.CLASS_NAME,
                        KeyValueField.NAME.toCanonicalValue(), entry.getKey(),
                        KeyValueField.VALUE.toCanonicalValue(), entry.getValue()
                );

                orientGraph.addEdge(null, outVertex, inVertex, Content.EDGE_CONTAINS);
            }
        }
    }

    /**
     * Listen to {@link org.randombits.confluence.metadata.event.MetadataUpdatedEvent}.
     *
     * @param metadataUpdatedEvent
     */
    @EventListener
    public void onEvent(MetadataUpdatedEvent metadataUpdatedEvent) {
        buildIndex(
            metadataUpdatedEvent.getMetadataStorage().getContent().getIdAsString(),
            metadataUpdatedEvent.getMetadataStorage().getBaseMap()
        );
    }

    /**
     * Listen to events from other nodes in clustered environment.
     *
     * @param clusterEventWrapper
     */
    @EventListener
    public void onEvent(ClusterEventWrapper clusterEventWrapper) {
        Event event = clusterEventWrapper.getEvent();
        if (event instanceof MetadataUpdatedEvent) {
            onEvent((MetadataUpdatedEvent) event);
        }
    }

    @Autowired
    public void setDatabaseBean(DatabaseBean databaseBean) {
        this.databaseBean = databaseBean;
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}