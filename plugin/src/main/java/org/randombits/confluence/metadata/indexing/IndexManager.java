package org.randombits.confluence.metadata.indexing;

import java.util.List;
import java.util.Map;

/**
 * Interface to interact with metadata index.
 *
 * @author kaifung
 * @since 7.0.0.20150209
 */
public interface IndexManager {

    /**
     * Retrieve the indexed String type value of specified name on a specified content.
     *
     * @param contentId
     * @param fieldName
     * @param clazz Type of the field to look for, e.g. passing String.class will limit the query to string type fields.
     * @return Value of type T.
     * @throws IllegalArgumentException when {@literal clazz} type is not supported.
     * @throws FieldNotFoundException when fieldName cannot be found.
     */
    <T> T query(String contentId, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException;

    /**
     * Retrieve the indexed String type value of specified name on a specified content.
     *
     * @param contentId
     * @param path
     * @param fieldName
     * @param clazz Type of the field to look for, e.g. passing String.class will limit the query to string type fields.
     * @return Value of type T.
     * @throws IllegalArgumentException when {@literal clazz} type is not supported.
     * @throws FieldNotFoundException when fieldName cannot be found.
     */
    <T> T query(String contentId, List<String> path, String fieldName, Class<? extends T> clazz) throws IllegalArgumentException, FieldNotFoundException;

    /**
     * Build the index of specified content with specified metadata.
     *
     * @param contentId
     * @param metadata
     */
    void buildIndex(String contentId, Map<String, Object> metadata);
}
