package org.randombits.confluence.metadata.migration;

import com.atlassian.confluence.pages.Attachment;
import com.google.common.base.Function;
import org.randombits.confluence.metadata.reference.AttachmentReference;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @author Mohd Faiz Hasim
 * @author LongYC
 * @since 6.1.0
 */
@Component
public class AttachmentReferenceMigrator {

    /**
     * Append attachmentId into AttachmentReference
     * so that the ID can be used as identifier instead of filename.
     * AttachmentReference must be inside a List as of current Metadata implementation.
     *
     * @param stringObjectMap String-Object map
     * @param attachment Confluence Attachment object
     */
    public Map process(Map<String, Object> stringObjectMap, Attachment attachment) {
        for (Map.Entry entry : stringObjectMap.entrySet()) {
            processIfList(attachment).apply(entry);
            processIfMap(attachment).apply(entry);
        }

        return stringObjectMap;
    }

    private Function<Map.Entry<String, Object>, Map.Entry<String, Object>> processIfList(final Attachment attachment) {
        return new Function<Map.Entry<String, Object>, Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> apply(Map.Entry<String, Object> entry) {
                if (entry.getValue() instanceof List) {
                    updateAttachmentReference((List) entry.getValue(), attachment);
                }
                return entry;
            }
        };
    }

    private Function<Map.Entry<String, Object>, Map.Entry<String, Object>> processIfMap(final Attachment attachment) {
        return new Function<Map.Entry<String, Object>, Map.Entry<String, Object>>() {
            @Override public Map.Entry<String, Object> apply(Map.Entry<String, Object> entry) {
                if (entry.getValue() instanceof Map) {
                    process((Map) entry.getValue(), attachment);
                }
                return entry;
            }
        };
    }

    private void updateAttachmentReference(List valueObjectList, Attachment attachment) {
        ListIterator iterator = valueObjectList.listIterator();

        while (iterator.hasNext()) {
            Object valueObject = iterator.next();
            if (valueObject instanceof AttachmentReference) {
                AttachmentReference attachmentReference = (AttachmentReference) valueObject;
                if (attachmentReference.getFileName().equals(attachment.getFileName())) {
                    iterator.set(new AttachmentReference(attachment));
                }
            }
        }
    }
}