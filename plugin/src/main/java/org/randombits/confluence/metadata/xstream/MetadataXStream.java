/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.alias.ClassMapper;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import org.randombits.confluence.metadata.MetadataMap;

/**
 * Extension of the XStream object which allows for more graceful handling of unrecognised objects. 
 *
 * @author David Peterson
 */
public class MetadataXStream extends XStream
{
    private static final String METADATA_MAP_ALIAS = "metadata";

    public MetadataXStream()
    {
        super();
    }

    public MetadataXStream(HierarchicalStreamDriver hierarchicalStreamDriver)
    {
        super(hierarchicalStreamDriver);
    }

    public MetadataXStream(ReflectionProvider reflectionProvider)
    {
        super(reflectionProvider);
    }

    public MetadataXStream(ReflectionProvider reflectionProvider, HierarchicalStreamDriver hierarchicalStreamDriver)
    {
        super(reflectionProvider, hierarchicalStreamDriver);
    }

    public MetadataXStream(ReflectionProvider reflectionProvider, ClassMapper mapper, HierarchicalStreamDriver hierarchicalStreamDriver)
    {
        super(reflectionProvider, mapper, hierarchicalStreamDriver);
    }

    @Override protected void setupDefaultImplementations()
    {
        super.setupDefaultImplementations();

        //this.addDefaultImplementation(MetadataMap.class, Map.class);
    }

    @Override protected void setupAliases()
    {
        super.setupAliases();
        alias(METADATA_MAP_ALIAS, MetadataMap.class);
    }

    @Override protected void setupConverters()
    {
        super.setupConverters();
        registerConverter(new MetadataMapConverter(getClassMapper()));
        /* TODO: Re-enable this once Confluence upgrades to XStream 1.2
        registerConverter(new PlaceholderConverter());
        */
    }

/*
    protected MapperWrapper wrapMapper(MapperWrapper next) {
        return new MapperWrapper(next) {
            public Class realClass(String name) {
                Class realClass;
                try {
                    realClass = super.realClass(name);
                } catch (CannotResolveClassException e) {
                    realClass = null;
                }

                if (realClass == null)
                {
                    realClass = UnrecognisedObject.class;
                }
                return realClass;
            }
        };
    }
*/
}
