package org.randombits.confluence.metadata.impl;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;
import org.randombits.confluence.metadata.TypeHandler;

/**
 * Allows definitions of &lt;environment-provider&gt; elements.
 */
public class TypeHandlerModuleDescriptor extends AbstractModuleDescriptor<TypeHandler> {

    private TypeHandler typeHandler;

    int weight = 0;

    public TypeHandlerModuleDescriptor( ModuleFactory moduleFactory ) {
        super( moduleFactory );
    }

    @Override
    public void init( @NotNull Plugin plugin, @NotNull Element element ) throws PluginParseException {
        super.init( plugin, element );
    }

    @Override
    public TypeHandler getModule() {
        if ( typeHandler == null ) {
            typeHandler = moduleFactory.createModule( moduleClassName, this );
        }
        return typeHandler;
    }
}
