package org.randombits.confluence.metadata.event;

import com.atlassian.confluence.event.events.ConfluenceEvent;
import com.atlassian.confluence.event.events.cluster.ClusterEvent;
import org.randombits.confluence.metadata.MetadataStorage;

/**
 * Event to fire when a metadata is updated (to indicate that, e.g. indexes on other nodes need update).
 * Refer to:
 * https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/how-do-i-ensure-my-add-on-works-properly-in-a-cluster#HowdoIensuremyadd-onworksproperlyinacluster
 *
 * @author LongYC
 * @since 7.0.0.20150209
 */
public class MetadataUpdatedEvent extends ConfluenceEvent implements ClusterEvent {

    private final MetadataStorage metadataStorage;

    public MetadataUpdatedEvent(MetadataUpdatedEventEmitter eventSource, MetadataStorage metadataStorage) {
        super(eventSource);
        this.metadataStorage = metadataStorage;
    }

    public MetadataStorage getMetadataStorage() {
        return metadataStorage;
    }
}