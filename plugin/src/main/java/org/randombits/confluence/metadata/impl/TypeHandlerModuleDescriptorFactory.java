package org.randombits.confluence.metadata.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

/**
 * Constructs a new {@link TypeHandlerModuleDescriptor}s.
 */
public class TypeHandlerModuleDescriptorFactory extends SingleModuleDescriptorFactory<TypeHandlerModuleDescriptor> {

    private final ModuleFactory moduleFactory;

    public TypeHandlerModuleDescriptorFactory( HostContainer hostContainer, ModuleFactory moduleFactory ) {
        super( hostContainer, "metadata-type-handler", TypeHandlerModuleDescriptor.class );
        this.moduleFactory = moduleFactory;
    }


    @Override
    public ModuleDescriptor getModuleDescriptor( String type ) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        return hasModuleDescriptor( type ) ? new TypeHandlerModuleDescriptor( moduleFactory ) : null;
    }
}
