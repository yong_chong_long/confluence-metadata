package org.randombits.confluence.metadata.reference;

public class EvaluatedNumberReference implements Reference {


    private static final long serialVersionUID = -534743642815834449L;

    private long contentId;

    private String dataPath;

    private String expression;

    /**
     * Used by XStream.
     */
    @SuppressWarnings({"UnusedDeclaration"})
    EvaluatedNumberReference() {
    }

    public EvaluatedNumberReference( long contentId, String dataPath, String expression ) {
        this.contentId = contentId;
        this.dataPath = dataPath;
        this.expression = expression;
    }

    public long getContentId() {
        return contentId;
    }

    public String getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression;
    }

    public String getDataPath() {
        return dataPath;
    }
}