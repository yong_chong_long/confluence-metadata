package org.randombits.confluence.metadata.indexing.graph;

import com.atlassian.confluence.setup.BootstrapManager;
import com.orientechnologies.orient.core.Orient;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.io.File.separator;

/**
 * Manage the lifecycle of database connections.
 *
 * @author LongYC
 * @since 7.0.0.20150209
 */
@Component
public class DatabaseBean implements InitializingBean, DisposableBean {

    public static final String DATABASE_NAME = "metadataIndexDB";

    private BootstrapManager bootstrapManager;
    private OrientGraphFactory orientGraphFactory;
    private OrientGraph graph;

    @Override
    public void afterPropertiesSet() throws Exception {
        orientGraphFactory = new OrientGraphFactory(getDatabaseDirectory()).setupPool(1, 10);
        graph = orientGraphFactory.getTx();
    }

    @Override
    public void destroy() throws Exception {
        graph.shutdown();
        Orient.instance().shutdown();
        orientGraphFactory.close();
    }

    public OrientGraph getGraph() {
        return graph;
    }

    public String getDatabaseDirectory() {
        return "plocal:" + bootstrapManager.getLocalHome() + separator + "com.servicerocket.confluence.metadata" + separator + DATABASE_NAME;
    }

    @Autowired
    public void setBootstrapManager(BootstrapManager bootstrapManager) {
        this.bootstrapManager = bootstrapManager;
    }
}