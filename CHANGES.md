Changes
=======

2015-04-07, 7.1.0
-----------------
 * WIKI-189 Fix metadata does not index non-root level fields.

2015-03-25, 7.0.0
-----------------
 * WIKI-140 Initial implementation of indexing for key-value pair string type fields.
 * WIKI-175 Fix Data rollback and NPE for indexing.

2014-12-17, 6.1.1
-----------------
 * WIKI-122 Compatibility with Comala workflows.

2014-11-19, 6.1.0
-----------------
 * AN22-964 Use ID instead of filename as identifier for attachment references.
 * AN22-865 Compatibility for Confluence data center.

2014-10-15, 6.0.6
-----------------
 * WIKI-9 Compatibility with Confluence 5.6

2014-07-02, 6.0.5
-----------------
 * AN22-712 Fix Scaffolding {get-data} is not getting the latest value from {set-data}

2014-06-26, 6.0.4
-----------------
 * Add saveSetData method to handle saving of {set-data} value in metadata

2014-06-19, 6.0.3
-----------------
 * Remove text indexing `Extractor2` implementation and move it into Scaffolding codebase instead.

2014-02-19, 6.0.2
-----------------
 * Fix issue whereby users are assumed to be conclusively ConfluenceUser or UnknownUser

2014-02-18, 6.0.1
-----------------
 * Restore `username` property in `UserReference` to be backward compatible with old and/or migration-failed data.

2013-12-06, 6.0.0
------------------
 * Implement user key to support username rename in Confluence 5.3


2013-06-06, 5.0.7 
-----------------
 * Fix invalid serialization in the clustering environment
